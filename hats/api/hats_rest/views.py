from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from hats_rest.models import Hat


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "name",
        "fabric",
        "style",
        "color",
        "silliness",
        "picture",
    ]

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatEncoder,
            safe=False,
        )
