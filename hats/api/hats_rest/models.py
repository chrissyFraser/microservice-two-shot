from django.db import models


class Hat(models.Model):
    name = models.CharField(max_length=200, unique=True)
    fabric = models.CharField(max_length=200, null=False, blank=True)
    style = models.CharField(max_length=200, null=False, blank=True)
    color = models.CharField(max_length=100, blank=True, null=False)
    silliness = models.PositiveSmallIntegerField(null=False)
    picture = models.URLField(null=True)
    # location = models.ForeignKey(Location, related_name=)
    # finish this later

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name", )