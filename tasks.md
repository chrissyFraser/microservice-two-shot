---
Tasks:

[x] You need to install your Django app into the Django project for your microservice. That's the INSTALLED_APPS deal.

[x] Then, try making the simplest model possible, not worrying about integration until later.

[x] Make one function view to show the list of your model. Whenever you need a point of reference about how to do something for the RESTful APIs, look at your code in Conference GO! or the Wardrobe API in the project.

[x] Configure the view in a URLs file that you will have to create for your Django app.

[x] Include the URLs from your Django app into your Django project's URLs.

[x] See if you can use Insomnia to see an empty list of your resources.

[x] Follow the steps for Git to get your code in the main branch based on Git In a Group.

[x] Add to the function to handle POST requests to create a new record in your database. Use Insomnia to see if you can POST data to create a record. Then, use the GET to see if it's now in the list.

[x] Follow the steps for Git to get your code in the main branch based on Git In a Group.

### Seems like this is an important step v

[x] Now, try to build a React component to fetch the list and show the list of your resources, hats or shoes, in the Web page. Try to make it pretty, if you'd like. The React app has Bootstrap already in it. Route your list component to the appropriate path to show up when you click on the navigation bar link. This is very much like either the AttendeesList component or the MainPage component from Conference GO! But, you will make your own list component, like ShoeList or HatList and use that.

[] Follow the steps for Git to get your code in the main branch based on Git In a Group.

### Seems like I'll spend a lot of time here v

[] Add add, view, and delete your resource using the React UI using your microservice's back-end. Each time you succeed at something, follow the steps for Git to get your code in the main branch based on Git In a Group. If you're doing the dev branch strategy, merge your working code to main.

[] At some point, write polls to get the Bin or Location data for your microservice. Use the requests library to make the HTTP request to the Wardrobe API, then loop over it and add it to your database. This is like when you polled for Account information from the attendees microservice in Conference GO!

[] Leave the "delete" functionality for last. Then, add a delete button for each item in your list. If you give that button an onClick handler, you can use that to make a fetch with the HTTP method DELETE. You'll just need to figure out how to get the id of the thing you want to delete for the URL. To solve this, you could do something like this for the onClick handler, assuming you have a variable named hat that contains the Hat data.


#<button onClick={() => this.delete(hat.id)}>


---
