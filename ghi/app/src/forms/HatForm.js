import React from 'react';



class HatForm extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            fabric: '',
            style: '',
            color: '',
            silliness: '',
            picture: '',
            location: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleSillinessChange = this.handleSillinessChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    
    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value })
    }

    handleStyleChange(event) {
        const value = event.target.value;
        this.setState({ style: value })
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value })
    }

    handleSillinessChange(event) {
        const value = event.target.value;
        this.setState({ silliness: value })
    }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8090/api/hats/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                const option = document.createElement('option');
                option.value = location.id //??
                option.innerHTML = location.id //??
                selectTag.appendChild(option);
            }
        }
    }


}

export default HatForm;