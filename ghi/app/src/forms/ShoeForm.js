import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            "color": '',
            "brand": '',
            "style": '',
            "image": '',
        };

        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleBrandChange = this.handleBrandChange.bind(this);
        this.handleStyleChange = this.handleStyleChange.bind(this);
        this.handleImageChange = this.handleImageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ shoes: data.shoes });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        const shoeUrl = 'http://localhost:8000/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoes = await response.json();
            console.log(newShoes);
            this.setState({
                "color": '',
                "brand": '',
                "style": '',
                "image": '',
            });
        }
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangeBrand(event) {
        const value = event.target.value;
        this.setState({ brand: value });
    }

    handleChangeStyle(event) {
        const value = event.target.value;
        this.setState({ style: value });
    }

    handleChangeImage(event) {
        const value = event.target.value;
        this.setState({ image: value });
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a New Shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.color} onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.brand} onChange={this.handleBrandChange} placeholder="Brand" required type="number" name="brand" id="brand" className="form-control" />
                                <label htmlFor="brand">Brand</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.style} onChange={this.handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                                <label htmlFor="style">Style</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.image} onChange={this.handleImageChange} placeholder="Image" required type="text" name="image" id="image" className="form-control" />
                                <label htmlFor="image">Image</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default ShoeForm;