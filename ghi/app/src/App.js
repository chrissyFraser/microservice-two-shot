import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import ShoeForm from './forms/ShoeForm';
import HatList from './hatList'
import HatForm from './forms/HatForm'

function App(props) {
  if (props.shoes === undefined && props.hats === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
        </Routes>
      </div>
      <div>
        <Routes>
          <Route path="hats">
            <Route index element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
      <div className="container">
        <Routes>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm />} />
            <Route index element={<ShoesList shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
