import React from "react"

function ShoesList(props) {
    const [Shoe, setShoe] = useState(props.shoes[0]);
    const [shoeBin, setShoeBin] = useState();
    const shoeUrl = "http//localhost:8080/api/shoes/${Shoe.id}/";

    const shoeDetails = async () => {
        const response = await fetch(shoeUrl);

        if (response.ok) {
            const data = await response.json();
            setShoeBin(data.bin.closet_name);
        }
    }

    const deleteShoe = async () => {
        fetch(shoeUrl, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        })
        window.bin.reload();
    }

    const handleClick = async (Shoe) => {
        setShoe(Shoe);
        shoeDetails();
    }


    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Color</th>
                    <th>Brand</th>
                    <th>Style</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoes => {
                    return (
                        <tr key={shoes.id}>
                            <td>{shoes.color}</td>
                            <td>{shoes.brand}</td>
                            <td>{shoes.style}</td>
                            <td><img src={shoes.image} alt="shoe_image" /></td>
                            <td>
                                <button onClick={() => handleClick(deleteShoe)} type="button">Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoesList;