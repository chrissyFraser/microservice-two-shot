from django.db import models


class Shoes(models.Model):
    style = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    brand = models.CharField(max_length=50)
    image = models.URLField(null=True, blank=True)

    def __str__(self):
        return f"{self.color} {self.brand} {self.style}"

    class Meta:
        ordering = ("style", )


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    bin_size = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)
