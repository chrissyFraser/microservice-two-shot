from django.urls import path
from shoes_rest.views import api_shoes

urlpatterns = [
    path('shoes/', api_shoes, name="api_shoes"),
]
